#!/bin/sh
# Do initial configuration of freeradius

set -e

CONF_DIR = '/etc/freeradius'
BASEDIR=`dirname $0`

service freeradius stop

cp "$CONF_DIR/sites-available/default" "$CONF_DIR/sites-available/default.backup"
cp "$BASEDIR/radius-default.conf" "$CONF_DIR/sites-available/default"

service freeradius start




