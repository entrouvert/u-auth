# This file is sourced by "execfile" from u-auth.settings

import os

from django.conf import global_settings

DEBUG = False
TEMPLATE_DEBUG = False

PROJECT_NAME = 'u-auth'

EMAIL_SUBJECT_PREFIX = '[%s] ' % PROJECT_NAME

ETC_DIR = '/etc/%s' % PROJECT_NAME
VAR_DIR = '/var/lib/%s' % PROJECT_NAME

# collecstatic destination
STATIC_ROOT = os.path.join(VAR_DIR, 'collectstatic')

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

METADATAS_DIR = os.path.join(VAR_DIR, 'metadatas')

SECRET_KEY = file('/etc/%s/secret' % PROJECT_NAME).read()

ORGANIZATIONS_DIR = os.path.join(VAR_DIR, 'organizations')

execfile(os.path.join(ETC_DIR, 'settings.py'))
