import os

DEBUG = False
TEMPLATE_DEBUG = False

ALLOWED_HOSTS = [
        '*',
]

LANGUAGE_CODE = 'fr-fr'
TIME_ZONE = 'Europe/Paris'

# LDAP_CONF = {
#     'url': 'ldap://localhost',
#     'bind_dn': 'cn=admin,dc=dev,dc=entrouvert,dc=org',
#     'options': {},
#     'bind_passwd': 'changeme',
#     'dn': 'ou=users,dc=dev,dc=entrouvert,dc=org',
# }

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(VAR_DIR, 'db.sqlite3'),
    }
}
