% Installation et configuration
% UAuth: Portail captif dans le Cloud
% Entr'ouvert SCOP -- http://www.entrouvert.com

Installation
============

U-Auth est  fourni sous  forme d'un paquet debian téléchargeable.

Pour l'installer il est nécessaire de rajouter le depot d'Entrouvert dans votre
gestionnaire de paquets:
<pre>
deb http://deb.entrouvert.org/ wheezy-testing main
</pre>
ainsi que rajouter la clé GPG:
<pre>
wget -O - http://deb.entrouvert.org/entrouvert.gpg | sudo apt-key add -
</pre>

Installer le paquet:
<pre>
sudo apt-get install u-auth
</pre>
\ ![images/uauth_install.png](images/uauth_install.png)

les paquets *freeradius* ainsi que *slapd* seront installés.

Lors de la premiere installation u-auth demande la configuration de l'annuaire LDAP:

\ ![images/ldap_config.png](images/ldap_config.png)

une base ainsi qu'un compte administrateur seront crées.

La configuration du serveur radius sera faite automatiquement afin qu'il puisse communiquer avec le serveur ldap.

Configuration du serveur web
============================

Le paquet fourni un exemple de fichier de configuration pour le serveur web
nginx qui se trouve dans */usr/share/doc/u-auth/nginx-example.conf*. Ce fichier
peut être utilisé en modifiant le nom du serveur ainsi que les certificats:

<pre>
server_name u-auth.example.org
...
ssl_certificate         /etc/ssl/u-auth.example.org.pem;
ssl_certificate_key     /etc/ssl/u-auth.example.org.key;
</pre>

Parametrage de l'application
============================

La configuration de l'application se trouve dans le fichier
*/etc/u-auth/settings.py*.

Ce fichier contient, au format du langage Python, les parametres de connexion à
la base des données, nécessaire pour stocker les comptes locaux, les
identifiants de connexion à l'annuaire LDAP.

Base des données
----------------

Le système de gestion des bases des données par défaut est sqlite.

```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(VAR_DIR, 'db.sqlite3'),
    }
}
```

Un autre SGBD peut-être configuré en changeant les valeurs de *ENGINE* et
*NAME* ainsi qu'en rajoutant les paramètres de connexion:

```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'db_name',
        'USER': 'db_user',
        'PASSWORD': 'db_user_password',
        'HOST': ''
    }
}
```

Annuaire LDAP
-------------

Afin que l'application puisse communiquer avec l'annuaire LDAP et écrire les
données utilisateur, un compte ayant le droit d'écriture dans l'arbre utilisé
par le serveur radius doit être défini. Par exemple:
```python
LDAP_CONF = {
    'url': 'ldap://localhost',
    'bind_dn': 'uid=admin,ou=people,dc=entrouvert,dc=org',
    'options': {},
    'bind_passwd': '[mot de passe défini lors du parametrage du serveur ldap]',
    'dn': '[dn defini lors du parametrage du serveur ldap]'
}
```

#### Important!!!
Après la manipulation du fichier */etc/u-auth/settings.py* l'application doit être redémarrée:
<pre>
sudo service u-auth restart
</pre>


Outils de gestion des serveurs freeradius et slapd
--------------------------------------------------

Des outils de configuration et re-initialisation des serveurs freeradius et
slapd sont fournis dans l'application:

* reset-slapd
* setup-slapd
* setup-radius

Ils peuvent être executés avec la commande */usr/lib/u-auth/u-auth* suivie du nom de l'outil.
Par exemple:

```
/usr/lib/u-auth/u-auth reset-slapd
```

La page d'aide de l'outil est affichée en préfixant son nom par *help*:
```
/usr/lib/u-auth/u-auth help setup-slapd
```


