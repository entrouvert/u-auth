% Usage et personnalisation
% UAuth: Portail captif dans le Cloud
% Entr'ouvert SCOP -- http://www.entrouvert.com

Thème
=========

Il est possible de personnaliser la page d'accueil d'une organisation en
surchageant le templates et en ajoutant des feuilles des styles, images, etc.

Ces fichiers peuvent être chargés dans le compartiment *Theme* de l'interface
de gestion:

\ ![images/u-auth_management.png](images/u-auth_management.png)

Templates
---------
Seule la page d'accueil d'une organisation est personnalisable.

Cela peut être fait en chargeant un fichier template *base.html*.  C'est un
fichier doit contenir des balises
Django(https://docs.djangoproject.com/en/dev/topics/templates/) définissant la
structure personnalisée de la page.

Il doit impérativement définir le block:

```html
{% block content %}
{% endblock %}
```

utilisé pour afficher la liste des fournisseurs d'identité et la formulaire de
connexion invité.

Feuilles de styles et les statics
---------------------------------

Des feuilles de style peuvent être chargées par le fichier *base.html* défini
précedemment de façon suivante:

```html
{% load staticfiles %}
...
<head>
    ...
    <link href="{% static "entrouvert/eo.css" %}" media="all" rel="stylesheet" type="text/css" />
    ...
</head>
```

Afin que le fichier de style personnalisé *eo.css* soit chargé, il doit être
préfixé par le *slug* de l'organisation dont le thème est modifié.

Si l'organisation est *psl*, le static doit être défini de façon suivante:

```html
{% static "psl/eo.css" %}
```

Les images peuvent être chargées au même endroit que les feuilles de style et utilisées de la même façon dans le template:
```html
<img src="{% static "psl/logo.jpg" %}" />
```

Un exemple de thème est fourni dans le paquet debian sous */usr/share/u-auth/theme-example*, dont le rendu est le suivant:

\ ![images/eo_theme.png](images/eo_theme.png)


Federations
===========

La liste des fournisseurs d'identité auprès desquels les utilisateurs peuvent
être authentifiés peut être définie en chargeant des fichiers .xml des
fédérations dans l'onglet *Fédérations* sur l'interface de gestion.

Les fournisseurs d'identité présents dans ces fichiers seront affichés sur la
page d'accueil.


Comptes locaux
==============

Des comptes locaux peuvent être définis dans l'interface de gestion afin de
permettre l'authentification sans utiliser un fournisseur d'identité.

Les comptes peuvent être créés un par un ou plusieurs à la fois.

\ ![images/create_users.png](images/create_users.png)

Le champ *mot de passe* peut être omis lors de la création du compte, il sera
auto-généré. Il peut ensuite être visualisé en cliquant sur le nom de l'utilisateur:

\ ![images/local_user_details.png](images/local_user_details.png)

Un compte utilisateur peut être desactivé à tout moment, soit une date
d'expiration peut être définie.

\ ![images/edit_local_account.png](images/edit_local_account.png)


Importer des comptes à partir d'un fichier
------------------------------------------
Les comptes locaux peuvent être également importé à partir d'un fichier *.csv*

\ ![images/import_users.png](images/import_users.png)
