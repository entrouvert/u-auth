% Configuration pfSense
% UAuth: Portail captif dans le Cloud
% Entr'ouvert SCOP -- http://www.entrouvert.com

Ce document spécifie les étapes de configuration d'un portail captif pfSense
pour son raccordement à la plateforme U-Auth.

Configuration d'un portail captif pfSense
=========================================

Dans le menu __Services/Captive Portal__

\ ![images/pfsense_home.png](images/pfsense_home.png)

ajouter une nouvelle zone:

\ ![images/pfsense_new_zone.png](images/pfsense_new_zone.png)


Configurer la zone ainsi créée:

1. activer la zone:

\ ![images/pfsense_activate_zone.png](images/pfsense_activate_zone.png)


2. configurer l'URL de redirection vers le page de connexion U-Auth:

\ ![images/pfsense_config_redirect_url.png](images/pfsense_config_redirect_url.png)

3. configurer l'authentification Radius:
 * protocole d'authentification: PAP
 * adresse IP du serveur U-Auth: 176.31.146.80
 * secret partagé: testing123

\ ![images/pfsense_radius_config.png](images/pfsense_radius_config.png)

4. définir un nom local pour le portail captif:

\ ![images/pfsense_server_name.png](images/pfsense_server_name.png)

5. desactiver le HTTPS Forwards

\ ![images/pfsense_disable_https_forwards.png](images/pfsense_disable_https_forwards.png)

6. personnaliser la page d'authentification du portail captif en chargeant un fichier html contenant obligatoirement la variable `$PORTAL_REDIRURL$`:

\ ![images/pfsense_portal_page.png](images/pfsense_portal_page.png)

Example de fichier:

```html
<html>
  <head>
  <title>You are being redirected to authentication page</title>
  </head>
  <body>
    <h3>You are being redirected to authentication page</h3>
    <p>If you are not redirected, please
    <a id="redirect" href="$PORTAL_REDIRURL$">click here</a></p>
    <script type="text/javascript">
      var redir = document.getElementById('redirect');
      redir.href += window.location.search;
      window.location.href="$PORTAL_REDIRURL$" + window.location.search;
    </script>
  </body>
</html>
```

7. Autoriser le portail captif à acceder à U-Auth et les fournisseurs d'identité de la fédération:

\ ![images/pfsense_allowed_ips.png](images/pfsense_allowed_ips.png)


8. Dans le resolver DNS local rajouter le nom et l'adresse locale du portail captif:

\ ![images/pfsense_dns_resolver.png](images/pfsense_dns_resolver.png)

\ ![images/pfsense_add_host.png](images/pfsense_add_host.png)


Test d'authentification
=======================

Depuis un poste interne au réseau du portail captif aller sur une page(par exemple http://perdu.com):

\ ![images/pfsense_test_login1.png](images/pfsense_test_login1.png)

La page personnalisée, rédirigeant vers U-Auth, sera affichée:

\ ![images/pfsense_test_redirect.png](images/pfsense_test_redirect.png)

Si l'accès à la plateforme U-Auth a été bien autorisée au niveau du portail captif, la page de votre organisme avec la liste des fournisseurs d'identité sera affichée:

\ ![images/uauth.png](images/uauth.png)

En choisisant un fournisseur d'identité, également autorisé au niveau du portail captif, la mire de connexion est affichée:

\ ![images/idp_test_renater.png](images/idp_test_renater.png)

\ ![/idp_psl_dev.png](images/idp_psl_dev.png)

Une fois authentifié auprès du fournisseur d'identité l'utilisateur est autorisé au niveau du portail captif et est renvoyée vers la page demandée initiallement(dans cet exemple http://perdu.com)

La session utilisateur sera visible dans le dashboard du portail captif:

\ ![images/pfsense_dashboard.png](images/pfsense_dashboard.png)



