import os
import json

from django.conf import global_settings

BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'jg@eap06#(68*jqv)j=q5we33*-5mqiku_r231++$2cdl43_bl'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'mellon',
    'gadjo',
    'uauth',
    'uauth.organization',
    'django_tables2',
)

METADATA_URIS = (
    'https://federation.renater.fr/test/renater-test-metadata.xml',
    # 'https://federation.renater.fr/renater/idps-renater-metadata.xml',
    # 'https://federation.renater.fr/edugain/idps-edugain-metadata.xml',
)

METADATAS_DIR = os.path.join(BASE_DIR, 'metadatas')

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = global_settings.TEMPLATE_CONTEXT_PROCESSORS + \
                              ('django.core.context_processors.request',
                               'uauth.context_processors.theme_base',)

ROOT_URLCONF = 'uauth.urls'

WSGI_APPLICATION = 'uauth.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'

STATICFILES_FINDERS = global_settings.STATICFILES_FINDERS + \
            ('gadjo.finders.XStaticFinder',)

LDAP_CONF = {
    'url': 'ldap://localhost',
    'bind_dn': 'cn=admin,dc=entrouvert,dc=org',
    'options': {},
    'bind_passwd': 'entrouvert42',
    'dn': 'ou=radius,dc=entrouvert,dc=org',
}

ORGANIZATIONS_DIR = os.path.join(BASE_DIR, 'organizations')

AUTHENTICATION_BACKENDS = global_settings.AUTHENTICATION_BACKENDS + (
    'mellon.backends.SAMLBackend',
    'uauth.backends.LocalAccountPasswordBackend',
)

MELLON_ATTRIBUTE_MAPPING = {
    'email': '{attributes[mail][0]',
    'first_name': '{attributes[gn][0]}',
    'last_name': '{attributes[sn][0]}',
}

# mellon authentication params
MELLON_ADAPTER = ('uauth.adapters.UAuthAdapter', )

MELLON_IDENTITY_PROVIDERS = []

MELLON_PRIVATE_KEY = """-----BEGIN PRIVATE KEY-----
MIIEwAIBADANBgkqhkiG9w0BAQEFAASCBKowggSmAgEAAoIBAQDH8DE3TT4xUbe4
/lTUNYM6cONGmESMGdLQGMPI3zO8ObslD7Nc2QtvBVz3hldZM/NZ9SAtvhoGwT3u
bXFWWUFkxC1F0qaz3Lc3XY5u2r4f2+aWGjdUE504fNmyZpAsPBSZcAEnMhweCGi0
d+RveaG8VRlHQDHzBcp/8fj4Nhfferv46i7GHUmoEJSZCbjMbCCgg1AOFqBRWQ8P
sQlwY9nGQq0RaQOpDvoBjZOkGBzcg1XqfgvZB3AwWMXaBoupxoCBEwbLllWB6Wb2
FT4mng0aOFPaKJabynCvdRpD5Fc9QR9Fhmzm+O+1eJbTj8RC8wJ/g8jdRRMLJATN
zAJzPnS5AgMBAAECggEBAJWXZptj1aZ856Mn1zw3FYpCMCn/fzn7kDlWbhX6ufCY
mSbvillroxxbvOUNxIjfT7H8ryjKVRq8tqOPiv76JO1Jwj29kDB+cS6hdxIF475e
fzXLQ55KVpWPBCpwhmoyQY9Tt+klmtf1nrF4CphFPvd0DEe9BiI4MPxthMPhZRc0
oKgOY5zcSvgbX042sLT2urlnOC2BU9ylUOt2GVIWhmj5mI6L+WGEmRPz4xtZyE32
hesvLPheFnWpm04xASlJ7Xq8DE2NzSkGsEEekzSgq+k0ntMMxgzYbA55EvioduIG
NrSAMSzThWBLEmXLu7fnPIq6A60DXcX9gmiG9leoVWkCgYEA4n89AAn3UMn0t1Pd
qwDJIhGwPFLak09YSLlokdZWoL6Bg25z4suewc+IC/BfEGWP9c2CeIdLcDQpYtLh
NmDyMUcv/+VLcwfUisvJpMVXOHZT9nB3A7L+qZpf6y1BzHYVNuFOgkJB1rX6d7w/
kfgEgWRAO4yZeZM6qqH9MKJ7FMMCgYEA4ftTeAozZq5mFlYhJd2OwonCtOVRPYqI
ckxDnj7iu5X955eCt/r9E01HqZmSdTT/EiAcCbseBcrA6y3B+8FqTQ0AEghU/72h
PhFjL3E7d5rgyIFIm5lPoZTXh0Uv55o8KWiOuGx+SHpA+JZ+LzQvQE4na30tkEsh
dB5fZtjcyNMCgYEAw5qK/budoGGpO6Hr78Bv80I/ZvlO1qVRYwzsJ/ZpPc0IYI9E
+XpMEqi9n0Hpd2mel/LxWFHPbok9rKDd6m5y+ue1plNJg4Ahp/Qyol/i4VofTNb3
kXRFxgcSCzI8fu0DiS+u84NxBJKISluCsvb0PSJs7oCQEpR5t97Jn/TSQq0CgYEA
0PYum8yK9UME4VipdDMohFkQs2dtNQP54f8Fc7ngvOcYjuN6G5g6FnQadQit9jWY
O3F3m8PsIhz3QUDhpGOYYUFJNGOB/a2jARrg5YjGHXYYry9lLYSZrudMWSgwAPCM
+xlVB9JC/9AUbgnzCsEthEAAkZOB43ClsZM4Uojrt8MCgYEAkdSR7m9FjY7KmmpC
XqSvgTl1Qjn4OmTnR1ASfrnRhQKPzl69ftQ21CP2H9IMPEEJ9CYwehxpCt/61lB2
nJnouXneXGasbXKD6tGUgUn3nLXSgZUFJsqSGQUxwNDBkkoAZyZsHZJgQwLJMUYK
FWK4A31CSSMhmyqTlqePGzOOat0=
-----END PRIVATE KEY-----"""

MELLON_PUBLIC_KEYS = ["""-----BEGIN CERTIFICATE-----
MIID7TCCAtWgAwIBAgIJAIMCYinZvykRMA0GCSqGSIb3DQEBCwUAMIGMMQswCQYD
VQQGEwJGUjETMBEGA1UECAwKU29tZS1TdGF0ZTEOMAwGA1UEBwwFUGFyaXMxEzAR
BgNVBAoMCkVudHJvdXZlcnQxHTAbBgNVBAMMFCouZGV2LmVudHJvdXZlcnQub3Jn
MSQwIgYJKoZIhvcNAQkBFhVzbWloYWlAZW50cm91dmVydC5jb20wHhcNMTUwMzE3
MTczMTE1WhcNMTYwMzE2MTczMTE1WjCBjDELMAkGA1UEBhMCRlIxEzARBgNVBAgM
ClNvbWUtU3RhdGUxDjAMBgNVBAcMBVBhcmlzMRMwEQYDVQQKDApFbnRyb3V2ZXJ0
MR0wGwYDVQQDDBQqLmRldi5lbnRyb3V2ZXJ0Lm9yZzEkMCIGCSqGSIb3DQEJARYV
c21paGFpQGVudHJvdXZlcnQuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIB
CgKCAQEAx/AxN00+MVG3uP5U1DWDOnDjRphEjBnS0BjDyN8zvDm7JQ+zXNkLbwVc
94ZXWTPzWfUgLb4aBsE97m1xVllBZMQtRdKms9y3N12Obtq+H9vmlho3VBOdOHzZ
smaQLDwUmXABJzIcHghotHfkb3mhvFUZR0Ax8wXKf/H4+DYX33q7+Oouxh1JqBCU
mQm4zGwgoINQDhagUVkPD7EJcGPZxkKtEWkDqQ76AY2TpBgc3INV6n4L2QdwMFjF
2gaLqcaAgRMGy5ZVgelm9hU+Jp4NGjhT2iiWm8pwr3UaQ+RXPUEfRYZs5vjvtXiW
04/EQvMCf4PI3UUTCyQEzcwCcz50uQIDAQABo1AwTjAdBgNVHQ4EFgQUUw8dVvEe
Nw6emo06xeqPIdxwO34wHwYDVR0jBBgwFoAUUw8dVvEeNw6emo06xeqPIdxwO34w
DAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQsFAAOCAQEAM4ZyCY0WIkkzozvO+Rw4
Q1bZmnz+F6+rZith852K57uRJU0GYX8Mz+LzQN+hqtWZipb6cwyX4I0TwVDkTHfA
blZlhiEterLLMYZlfVkgUpR56Z8SwE30yzDLAGvsBoY/Qy6sUFXGJMWNquWND2ii
7hYPP31xfv3omkQkkg4mxGWEpeo5oIwnM/tgPmlp3y8vACwkPXXzFkAUevjoqNXS
k5zCDjew+ZHe4d3Tzw2LUyRiELjOzdrjTtTCUQ3BZZSlLoXHdYdO+QZUUutaQMNa
3hWDMk5AYC2wkdbiFaYiihbz0MQrmoIc0RBl8kfdbQnC9xwoT1wgJeUOv0v2nuDv
iQ==
-----END CERTIFICATE-----"""]


local_settings_file = os.environ.get('UAUTH_SETTINGS_FILE',
        os.path.join(os.path.dirname(__file__), 'local_settings.py'))
if os.path.exists(local_settings_file):
    execfile(local_settings_file)
