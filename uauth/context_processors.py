import os

from django.conf import settings
from django.template.loader import get_template, TemplateDoesNotExist

def theme_base(request):
    base = get_template('uauth/base.html')

    if request.session.get('organization'):
        try:
            base = get_template('base.html', [os.path.join(settings.ORGANIZATIONS_DIR,
                                request.session['organization'], 'templates')])
        except TemplateDoesNotExist:
            pass

    return {'theme_base': base}
