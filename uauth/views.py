import json
import requests
from xml.etree import ElementTree

from django.views.generic.base import TemplateView
from django.views.generic import FormView
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render_to_response, get_object_or_404
from django.core import signing
from django.http.request import QueryDict
from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy as _

from mellon.views import LoginView as MellonLoginView

from .organization.models import Organization, LocalAccount
from .forms import GuestLoginForm, VoucherLoginForm
from .utils import create_radius_user, is_organization_idp, \
    get_idp_list


class HomeView(TemplateView):
    template_name = 'uauth/home.html'

homepage = HomeView.as_view()

class LoginMixin(object):
    def login(self, organization):
        context = {'organization': organization}
        result = create_radius_user()
        if result:
            username, password = result
            params = QueryDict(self.request.session[organization.slug], mutable=True)
            hotspot_url = organization.hotspot_url

            if 'login_url' in params:
                hotspot_url = params.pop('login_url')[0]

            context.update({'params':  params.urlencode(),
                            'hotspot_url': hotspot_url,
                            'data': {'username': username,
                                     'password': password}
                        })
            return render_to_response('uauth/%s_login_successful.html' % organization.hotspot_type,
                                          context)
        return render_to_response('uauth/login_failed.html', context)


class LoginView(LoginMixin, MellonLoginView):

    def authenticate(self, request, login, attributes):
        relayState = signing.loads(login.msgRelayState)
        organization = Organization.objects.get(slug=relayState['organization'])
        attr = attributes
        try:
            if 'eduPersonTargetedID' in attributes:
                attrkey = 'eduPersonTargetedID'
            else:
                attrkey = 'urn:oid:1.3.6.1.4.1.5923.1.1.1.10'
            eduPersonTargetedID_xml = ElementTree.fromstring(attributes[attrkey][0])
            eduPersonTargetedID = '%s' % eduPersonTargetedID_xml.text
            eduPersonTargetedID_NameQualifier = eduPersonTargetedID_xml.attrib['NameQualifier']
        except:
            eduPersonTargetedID_NameQualifier = attributes['issuer']

        if is_organization_idp(eduPersonTargetedID_NameQualifier, organization):
            return self.login(organization)

login = csrf_exempt(LoginView.as_view())


class OrganizationPageView(LoginMixin, FormView):
    form_class = GuestLoginForm
    template_name = 'uauth/organization.html'

    def get_context_data(self, **kwargs):
        context = super(OrganizationPageView, self).get_context_data(**kwargs)
        organization = get_object_or_404(Organization,
                                slug=self.kwargs['organization_slug'])
        idps = get_idp_list()
        self.request.session['organization'] = organization.slug
        self.request.session[organization.slug] = self.request.GET.urlencode()
        relay = signing.dumps({'organization': organization.slug})
        context.update({'idps': idps,
                        'guest_login_form': kwargs['form'],
                        'relay': relay,
                        'organization': organization,
                        'voucher_login_form': VoucherLoginForm()
                        })
        return context

    def form_valid(self, form):
        data = form.cleaned_data
        organization = get_object_or_404(Organization,
                                slug=self.kwargs['organization_slug'])
        data.update({'organization': organization})
        user = authenticate(**data)
        if user:
            return self.login(organization)
        else:
            form.add_error(None, _('Unknown or inactive user'))
            return self.form_invalid(form)

organization = OrganizationPageView.as_view()
