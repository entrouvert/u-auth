from mellon.adapters import DefaultAdapter

from .utils import get_idp_list


class UAuthAdapter(DefaultAdapter):
    def get_idps(self):
        return get_idp_list()

    def get_idp(self, entity_id):
        for idp in self.get_idps():
            if idp['ENTITY_ID'] == entity_id:
                return idp
