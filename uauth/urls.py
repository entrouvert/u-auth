from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.auth.decorators import login_required

from .views import homepage, organization, login
from .urls_utils import decorated_includes
from .organization.urls import urlpatterns as organization_urls

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^(?P<organization_slug>[\w-]+)/manage/', decorated_includes(login_required, include(organization_urls))),
    url(r'^$', homepage, name='organization-home'),
    url(r'^accounts/', include('django.contrib.auth.urls')),
    url(r'^accounts/mellon/login/$', login, name='mellon_login'),
    url(r'^accounts/mellon/', include('mellon.urls')),
    url(r'^(?P<organization_slug>[\w-]+)$', organization, name='organization-home'),
    url(r'^(?P<organization_slug>[\w-]+)/login', login,
        name='organization-login'),
)
