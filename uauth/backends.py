from .organization.models import LocalAccount
from datetime import datetime

class LocalAccountPasswordBackend(object):

    def authenticate(self, login, password, organization):
        try:
            account = LocalAccount.objects.get(username=login, active=True,
                                               organization=organization)
            if account.password == password:
                if not account.expired:
                    return account
        except LocalAccount.DoesNotExist:
            return None
