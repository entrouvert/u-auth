from django import forms
from django.utils.translation import ugettext_lazy as _

class GuestLoginForm(forms.Form):
    login = forms.CharField(label=_('Login'))
    password = forms.CharField(label=_('Password'), widget=forms.PasswordInput)


class VoucherLoginForm(forms.Form):
    voucher = forms.CharField(label=_('Voucher'))
