import os
import csv
import datetime

from django.conf import settings
from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, redirect

from django.views.generic.base import TemplateView
from django.views.generic.list import ListView
from django.views.generic.edit import FormView, UpdateView
from django.views.generic import DetailView, View
from django.contrib import messages

from django_tables2 import RequestConfig

from .utils import create_user, create_or_update_users
from .models import LocalAccount, Organization
from .forms import *
from .tables import AccountTable


class OrganizationMixin(object):

    def get_queryset(self):
        qs = super(OrganizationMixin, self).get_queryset()
        return qs.filter(organization__slug=self.kwargs['organization_slug'])

    def get_success_url(self):
        return reverse_lazy('manage-users', kwargs={'organization_slug': self.kwargs['organization_slug']})

    def get_context_data(self, *args, **kwargs):
        ctx = super(OrganizationMixin, self).get_context_data(*args, **kwargs)
        ctx['organization'] = Organization.objects.get(slug=self.kwargs['organization_slug'])
        return ctx

class ManageView(OrganizationMixin, TemplateView):
    template_name = 'organization/manage.html'

manage = ManageView.as_view()

class UsersPageView(OrganizationMixin, ListView):
    template_name = 'organization/users.html'
    model = LocalAccount

    def get_context_data(self, *args, **kwargs):
        context = super(UsersPageView, self).get_context_data(*args, **kwargs)
        table = AccountTable(context['object_list'])
        RequestConfig(self.request).configure(table)
        context['table'] = table
        return context

users = UsersPageView.as_view()


class UsersCreateView(OrganizationMixin, FormView):
    template_name = 'organization/create-users.html'
    form_class = LocalAccountCreateForm

    def form_valid(self, form):
        data = form.cleaned_data
        data['organization'] = Organization.objects.get(slug=self.kwargs['organization_slug'])
        accounts_number = data.pop('accounts_number')
        accounts_number_start = data.pop('accounts_number_start')
        if accounts_number_start < 1:
            accounts_number_start = 0

        username = data['username']
        if accounts_number > 1:
            for index in xrange(accounts_number_start, accounts_number + accounts_number_start):
                data['username'] = '%s-%s' % (username, index)
                if not create_user(data):
                    messages.error(self, request, _('Error while creating user %s') % data['username'])
                    break
            messages.info(self.request, _('%s users added successfully') % accounts_number)
        else:
            if create_user(data):
                messages.info(self.request, _('User "%s" successfully created') % data['username'])
            else:
                messages.error(self.request, _('Error occured while creating user "%s"') % data['username'])
        return super(UsersCreateView, self).form_valid(form)

create_users = UsersCreateView.as_view()


class ShowUserView(OrganizationMixin, DetailView):
    model = LocalAccount
    template_name = 'organization/view_user.html'

view_user = ShowUserView.as_view()


class UserEditView(OrganizationMixin, UpdateView):
    template_name = 'organization/edit_user.html'
    model = LocalAccount
    form_class = LocalAccountForm

    def form_valid(self, form):
        username = self.object.username
        if 'delete' in self.request.POST:
            self.object.delete()
            messages.info(self.request, _('Account "%s" successfully deleted') % username)
            return redirect(self.get_success_url())
        else:
            messages.info(self.request, _('Account "%s" successfully updated') % username)
            return super(UserEditView, self).form_valid(form)

edit_user = UserEditView.as_view()


class ImportUsersView(OrganizationMixin, TemplateView):
    form_class = UsersImportForm
    template_name = 'organization/import_users.html'

    def get_context_data(self, **kwargs):
        ctx = super(ImportUsersView, self).get_context_data(**kwargs)
        ctx['form'] = self.form_class()
        return ctx

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)
        context = self.get_context_data(**kwargs)
        context['form'] = form
        if form.is_valid():
            data = form.cleaned_data['users_file']
            dialect = csv.Sniffer().sniff(data.read(1024))
            data.seek(0)
            reader = csv.reader(data, dialect)
            reader.next()
            users = []
            for row in reader:
                try:
                    user = {'username': row[0].strip(),
                            'first_name': row[1].strip(),
                            'last_name': row[2].strip(),
                            'password': row[4].strip(),
                            'organization': context['organization']
                        }
                except IndexError:
                    # ignore wrong lines
                    continue
                try:
                    user['expiration_date'] = datetime.datetime.strptime(row[3], '%Y-%m-%d')
                except ValueError:
                    pass
                users.append(user)
            created, updated = create_or_update_users(users)
            if created:
                messages.info(request, _('%s accounts added') % created)
            if updated:
                messages.info(request, _('%s accounts updated') % updated)
            return redirect(self.get_success_url())
        else:
            return self.render_to_response(context)

import_users = ImportUsersView.as_view()


class ThemeView(OrganizationMixin, TemplateView):
    template_name = 'organization/theme.html'

    def get_success_url(self):
        return reverse_lazy('manage-theme', kwargs={'organization_slug': self.kwargs['organization_slug']})

    def get_context_data(self, **kwargs):
        ctx = super(ThemeView, self).get_context_data(**kwargs)
        organization = ctx['organization']
        templates_dir = os.path.join(settings.ORGANIZATIONS_DIR,
                                organization.slug, 'templates')
        statics_dir = os.path.join(settings.ORGANIZATIONS_DIR,
                                organization.slug, 'static')
        ctx['templates'] = []
        ctx['statics'] = []
        if os.path.exists(templates_dir):
            ctx['templates'] = os.listdir(templates_dir)
        if os.path.exists(statics_dir):
            ctx['statics'] = os.listdir(statics_dir)
        ctx['templates_dir'] = templates_dir
        ctx['statics_dir'] = statics_dir
        return ctx

theme = ThemeView.as_view()

class UploadMixin(object):
    template_name = "organization/upload.html"

    def get_context_data(self, **kwargs):
        ctx = super(UploadMixin, self).get_context_data(**kwargs)
        ctx['form'] = self.form_class()
        return ctx

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)
        context = self.get_context_data(**kwargs)
        context['form'] = form
        organization = context['organization']
        destination_dir = os.path.join(settings.ORGANIZATIONS_DIR,
                                       organization.slug, self.upload_dir)
        if form.is_valid():
            data = form.cleaned_data[self.filename_param]
            if not os.path.exists(destination_dir):
                os.makedirs(destination_dir)
            try:
                with open(os.path.join(destination_dir, data.name), 'w') as template:
                    template.write(data.read())
                messages.info(request, _('File "%s" successfully uploaded') % data.name)
            except OSError:
                messages.error(request, _('An error occured while uploading file "%s"') % data.name)
            return redirect(self.get_success_url())
        else:
            return self.render_to_response(context)


class TemplateUpload(UploadMixin, ThemeView):
    form_class = TemplateForm
    filename_param = 'template_file'
    upload_dir = 'templates'

template_upload = TemplateUpload.as_view()


class TemplateDelete(ThemeView):

    def get(self, request, *args, **kwargs):
        ctx = self.get_context_data(**kwargs)
        template = request.GET.get('template')
        if os.path.exists(os.path.join(ctx['templates_dir'], template)):
            try:
                os.remove(os.path.join(ctx['templates_dir'], template))
                messages.info(request, _('Template %s successfully removed') % template)
            except IOError:
                messages.error(request, _('An error occured while removing file %s') % template)
        else:
            messages.error(request, _('Unknown template %s') % template)
        return redirect(self.get_success_url())


template_delete = TemplateDelete.as_view()


class StaticUpload(UploadMixin, ThemeView):
    form_class = StaticForm
    filename_param = 'static_file'
    upload_dir = 'static'

static_upload = StaticUpload.as_view()


class StaticDelete(ThemeView):

    def get(self, request, *args, **kwargs):
        ctx = self.get_context_data(**kwargs)
        static = request.GET.get('static')
        if os.path.exists(os.path.join(ctx['statics_dir'], static)):
            try:
                os.remove(os.path.join(ctx['statics_dir'], static))
                messages.info(request, _('Static file %s successfully removed') % static)
            except IOError:
                messages.error(request, _('An error occured while removing file %s') % static)
        else:
            messages.error(request, _('Unknown static %s') % static)
        return redirect(self.get_success_url())

static_delete = StaticDelete.as_view()


class FederationsView(OrganizationMixin, TemplateView):
    template_name = 'organization/federations.html'

    def get_success_url(self):
        return reverse_lazy('manage-federations', kwargs={'organization_slug': self.kwargs['organization_slug']})

    def get_context_data(self, **kwargs):
        ctx = super(FederationsView, self).get_context_data(**kwargs)
        org = ctx['organization']
        federations_dir = os.path.join(settings.ORGANIZATIONS_DIR,
                                    org.slug, 'federations')
        ctx['federations_dir'] = federations_dir
        if os.path.exists(federations_dir):
            ctx['federations'] = os.listdir(federations_dir)
        else:
            ctx['federations'] = []
        return ctx

federations = FederationsView.as_view()


class FederationsAdd(UploadMixin, FederationsView):
    form_class = FederationForm
    filename_param = 'federation_file'
    upload_dir = 'federations'

federations_add = FederationsAdd.as_view()


class FederationsDelete(FederationsView):
    def get(self, request, *args, **kwargs):
        ctx = self.get_context_data(**kwargs)
        f = request.GET.get('federation')
        if os.path.exists(os.path.join(ctx['federations_dir'], f)):
            try:
                os.remove(os.path.join(ctx['federations_dir'], f))
                messages.info(request, _('Federation file %s successfully removed') % f)
            except IOError:
                messages.error(request, _('An error occured while removing file %s') % f)
        else:
            messages.error(request, _('Unknown federation file %s') % f)
        return redirect(self.get_success_url())

federations_delete = FederationsDelete.as_view()
