import random

from django.contrib.auth.hashers import make_password

from .models import LocalAccount


def create_password():
    return ''.join([random.choice('23456789ABCDEFGHJLMNPQRSTUVWXZabcdefghjkmnpqrstuvwxyz')
                for x in range(random.randint(6,9))])


def create_user(data):
    if not data['password']:
        data['password'] = create_password()
    try:
        user = LocalAccount.objects.create(**data)
        return user
    except:
        return False

def create_or_update_users(data):
    created = 0
    updated = 0
    for user in data:
        try:
            account = LocalAccount.objects.get(username=user['username'])
            if not user['password']:
                del user['password']
            account.__dict__.update(user)
            account.save()
            updated += 1
        except LocalAccount.DoesNotExist:
            if create_user(user):
                created += 1
    return created, updated
