from django.contrib import admin

from .models import Organization, LocalAccount

@admin.register(Organization)
class OrganizationAdmin(admin.ModelAdmin):
    list_display = ('name', 'hotspot_type', 'hotspot_url')

@admin.register(LocalAccount)
class LocalAccountAdmin(admin.ModelAdmin):
    list_display = ('username', 'creation_date', 'expiration_date')
    list_filter = ('organization', )
