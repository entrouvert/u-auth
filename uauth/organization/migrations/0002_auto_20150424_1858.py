# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='LocalAccount',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('username', models.CharField(max_length=128, verbose_name='Username')),
                ('first_name', models.CharField(max_length=64, null=True, verbose_name='First name', blank=True)),
                ('last_name', models.CharField(max_length=64, null=True, verbose_name='Last name', blank=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('expiration_date', models.DateTimeField(null=True, blank=True)),
                ('password', models.CharField(help_text='Leave empty to auto-generate', max_length=128, verbose_name='Password', blank=True)),
                ('description', models.TextField(null=True, verbose_name='Description', blank=True)),
                ('active', models.BooleanField(default=True, verbose_name='Active')),
                ('organization', models.ForeignKey(to='organization.Organization')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='temporaryaccount',
            name='organization',
        ),
        migrations.DeleteModel(
            name='TemporaryAccount',
        ),
        migrations.AlterUniqueTogether(
            name='localaccount',
            unique_together=set([('organization', 'username')]),
        ),
    ]
