# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Organization',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128, verbose_name='Name')),
                ('slug', models.SlugField(max_length=128)),
                ('hotspot_url', models.CharField(max_length=128, null=True, verbose_name='Hotspot url', blank=True)),
                ('hotspot_type', models.CharField(max_length=32, verbose_name='Hotspot type', choices=[(b'pfsense', b'pfSense'), (b'meraki', b'Cisco Meraki')])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OrganizationMember',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('epti', models.CharField(max_length=128)),
                ('entity_id', models.CharField(max_length=256)),
                ('customer', models.ForeignKey(to='organization.Organization')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TemporaryAccount',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('username', models.CharField(max_length=128, verbose_name='Username')),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('password', models.CharField(max_length=128, verbose_name='Password')),
                ('voucher', models.CharField(max_length=128, verbose_name='Voucher')),
                ('organization', models.ForeignKey(to='organization.Organization')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
