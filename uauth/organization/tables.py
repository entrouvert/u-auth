from django.utils.translation import ugettext as _

import django_tables2 as tables

from .models import LocalAccount

class AccountTable(tables.Table):
    username = tables.TemplateColumn(
        '<a href="{% url "edit-user" organization.slug record.pk %}" rel="popup"><i class="icon-edit"></i></a><a href="{% url "view-user" organization.slug record.pk %}">{{ record.username }}</a>',
        verbose_name=_('Username'))

    class Meta:
        model = LocalAccount
        attrs = {'class': 'main', 'id': 'user-table'}
        fields = ('username', 'active', 'first_name', 'last_name')
        empty_text = _('None')
