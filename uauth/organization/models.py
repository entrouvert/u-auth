from django.db import models
from django.utils.translation import ugettext as _
from django.utils.timezone import make_aware, get_current_timezone

from datetime import datetime


HOTSPOT_CHOICES = (
    ('pfsense', 'pfSense'),
    ('meraki', 'Cisco Meraki')
)


class Organization(models.Model):
    name = models.CharField(_('Name'), max_length=128)
    slug = models.SlugField(max_length=128)
    hotspot_url = models.CharField(_('Hotspot url'), max_length=128,
                                   null=True, blank=True)
    hotspot_type = models.CharField(_('Hotspot type'),
                                    choices=HOTSPOT_CHOICES,
                                    max_length=32)

    def __unicode__(self):
        return self.name


class OrganizationMember(models.Model):
    customer = models.ForeignKey(Organization)
    epti = models.CharField(max_length=128)
    entity_id = models.CharField(max_length=256)


class LocalAccount(models.Model):
    organization = models.ForeignKey(Organization)
    username = models.CharField(_('Username'), max_length=128)
    first_name = models.CharField(_('First name'), max_length=64,
                        null=True, blank=True)
    last_name = models.CharField(_('Last name'), max_length=64,
                        null=True, blank=True)
    creation_date = models.DateTimeField(auto_now_add=True, blank=True)
    expiration_date = models.DateTimeField(null=True, blank=True)
    password = models.CharField(_('Password'), max_length=128, blank=True,
                                help_text=_('Leave empty to auto-generate'))
    description = models.TextField(_('Description'), null=True,
                        blank=True)
    active = models.BooleanField(_('Active'), default=True)

    class Meta:
        unique_together = ('organization', 'username')

    def expired(self):
        if self.expiration_date:
            return self.expiration_date < make_aware(datetime.now(),
                                        get_current_timezone())
        return False

    def get_fullname(self):
        return '%s %s' % (self.first_name, self.last_name)
