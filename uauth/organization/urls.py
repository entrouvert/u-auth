from django.conf.urls import patterns, url

from .views import *

urlpatterns = patterns('',
    url(r'^$', manage, name='manage'),
    url(r'^users/?$', users, name='manage-users'),
    url(r'^users/create$', create_users, name='create-users'),
    url(r'^users/import$', import_users, name='import-users'),
    url(r'^users/(?P<pk>[\w]+)/$', view_user, name='view-user'),
    url(r'^users/(?P<pk>[\w]+)/edit$', edit_user, name='edit-user'),
    url(r'^theme/?$', theme, name='manage-theme'),
    url(r'^theme/template/upload$', template_upload, name='template-upload'),
    url(r'^theme/template/delete$', template_delete, name='template-delete'),
    url(r'^theme/static/upload$', static_upload, name='static-upload'),
    url(r'^theme/static/delete$', static_delete, name='static-delete'),
    url(r'^federations/$', federations, name='manage-federations'),
    url(r'^federations/add$', federations_add, name='federations-add'),
    url(r'^federations/delete$', federations_delete, name='federations-delete')
)
