from django.utils.translation import ugettext as _
from django import forms

from .models import LocalAccount


class LocalAccountForm(forms.ModelForm):

    class Meta:
        exclude = ('organization', )
        model = LocalAccount
        widgets = {
            'expiration_date': forms.DateInput(attrs={'class': 'datepicker'}),
            'password': forms.PasswordInput
        }

    def save(self):
        # save previous password
        old_password = self.initial.get('password')
        obj = super(LocalAccountForm, self).save(commit=False)
        if not self.cleaned_data.get('password'):
            obj.password = old_password
        obj.save()
        return obj


class LocalAccountCreateForm(LocalAccountForm):
    accounts_number = forms.IntegerField(_('Number of accounts to create'), required=False)
    accounts_number_start = forms.IntegerField(_('First number of multiple accounts'), required=False)


class UsersImportForm(forms.Form):
    users_file = forms.FileField(_('Users file'))


class TemplateForm(forms.Form):
    template_file = forms.FileField(_('Template file'))

class StaticForm(forms.Form):
    static_file = forms.FileField(_('Static file'))


class FederationForm(forms.Form):
    federation_file = forms.FileField(_('Federation file'))
